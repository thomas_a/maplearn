.. Mapping Learning's Installation

Install
=======

.. note::

    Mapping Learning is still in an early stage of development . Will you dare
    installing an *alpha* version of a software? Mapping Learning is worth
    it...

Installing
----------

If you do not understand the below lines, check `2. Before installing`_.

Anaconda (easiest)
~~~~~~~~~~~~~~~~~~

Just want to enjoy *Mapping Learning*? Type:

.. code-block:: bash

   conda install -c sympythy maplearn

PIP
~~~

You can also use PIP, but you may have to deal with some tricky dependancies 
(*ie* GDAL):

.. code-block:: bash

   pip install maplearn

From source code (for developpers/curious)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Want to contribute to the source code of Mapping Learning ? Or just curious ?
You can also install *maplearn* from source code:

.. code-block:: bash

   git clone https://bitbucket.org/thomas_a/maplearn.git
   cd maplearn
   pip install .

Then, you will be able to get last changes (in *maplearn* folder):

.. code-block:: bash

   # get source code updates
   git pull
   # upgrade maplearn
   pip install .

2. Before installing
--------------------

Mapping Learning is based on Python and you need a way to install properly its
dependancies (either Anaconda or PIP).

Using **conda** (Windows, Mac or Linux)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: image/logo_anaconda.png
    :align: right
    :height: 30px

First, install an Anaconda distribution:

   * either Anaconda (complete): https://www.anaconda.com/distribution/
   * or Miniconda (lighter): https://docs.conda.io/en/latest/miniconda.html
   
*Nb: if you wonder which one you should use, have a look on* 
https://docs.conda.io/projects/conda/en/latest/user-guide/install/download.html#anaconda-or-miniconda 

Then, start "Anaconda prompt" and type:

.. code-block:: bash

   conda install -c sympythy maplearn

Using system packages and **PIP** (Linux, Debian-based distributions)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, install pip and some dependancies (Blas, Lapack..):

.. code-block:: bash

   apt-get install python3-pip
   apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran


Then, you can also install some Python librairies:

.. code-block:: bash

   apt-get install python3-numpy python3-scipy python3-pandas python3-gdal
   apt-get install python3-sklearn python3-seaborn python3-markdown

*NB: you can install Python librairies thanks to system packages (apt) or Pip.
Using System packages should be easier and faster. PIP will bring libraries in
latest version.*

Finally, you can install *maplearn*:

.. code-block:: bash

   pip install maplearn
