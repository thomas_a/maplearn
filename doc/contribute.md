Contribute
==========

.. note::

    .. image:: ./image/logo_lGPL.png
        :align: left
        :width: 60px
    
    Mapping Learning is a **free, open-source application**, distributed under
    the lGPL v3 license. Feel free to contribute !


You do not have to know how to code, you can contribute by:

* Using it
* Tracing the issues or proposing improvements on the
  `bug tracker <https://bitbucket.org/thomas_a/maplearn/issues?status=new&status=open>`_
* Improving documentation

Feel free to contact me (alban.thomas@univ-rennes2.fr).

Documentation
-------------

The documentation is built from the source code (using sphinx) and is available
in PDF, epub and HTML. Up-to-date documentation is available at
https://maplearn.readthedocs.io/en/latest/ .

Source code
-----------

.. image:: ./image/logo_python.png
    :align: left
    :width: 40px

*Maplearn* is written in **Python**. The source code is available at 
https://bitbucket.org/thomas_a/maplearn/src/master/. You can simply download a
copy from `this link <https://bitbucket.org/thomas_a/maplearn/downloads/>`_ 
but using git you can easily get updates.

.. code-block:: bash

    git clone https://bitbucket.org/thomas_a/maplearn.git
    # then, to get updates
    git pull

Philosophy
----------

Wondering what you can expect from *Mapping Learning* software? The few points
below give the "philosophy" of the software:

* Mapping Learning should be able to be **used as you wish** (*freedom*)

Mapping Learning is usable whatever your environment (Windows, Linux or Mac)
and the way you want (graphical or online interface of commands, or
even write a Python script).

* Mapping Learning should help you to **learn machine learning**
  (*knowledge base*)

We learn from our mistakes. Mapping Learning will not prevent you from making
meaningless predictions but must help you to realize you are doing it wrong
(through advice, warnings ...).

* Mapping Learning should help you to **understand your data**
  (*visualization*)

Data visualization really matters. Mapping Learning will integrate all possible
means (not just graphics) to better understand your data and results.

* Mapping Learning should be **useful to everyone** (*openness*)

Mapping Learning was initially dedicated to remote sensing, but the
applications of machine learning are much larger. *Maplearn* allows you to use
your data whether they are geographic or not (text files, Excel, and more to
come).

* Mapping Learning should be **up to date**

Machine Learning evolves quickly and Mapping Learning will try to give you
access to the latest algorithms.

* Mapping Learning is about machine learning and **only machine learning**

Mapping Learning is not a GIS or data manipulation software (ETL). Very good
software already exists.


Thanks
------

.. image:: ./image/logo_UR2.png
    :align: left
    :width: 40px
    
`Rennes 2 University <https://international.univ-rennes2.fr/>`_

|

.. image:: ./image/logo_LETG.png
    :align: left
    :width: 40px
   
`LETG - UMR6554 <http://letg.cnrs.fr/>`_

|
|
|

.. image:: ./image/logo_AFPy.svg
    :align: left
    :width: 40px

`AFPy - Association Francophone Python <https://www.afpy.org/>`_
