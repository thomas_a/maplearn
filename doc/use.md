.. Mapping Learning How to use

Use
===

Mapping Learning aims lets you choose how to use it:

* as a *"classical"* software (**GUI** - Graphical User Interface)
* typing in a terminal, with a **CLI** (Command Line Interface )
* as a Python library

.. note::

    * Whichever interface you choose, **configuration is the same**. Check
      :mod:`maplearn.app.config` for more details.
    * Machine Learning concepts are explained in :mod:`maplearn.ml`


This page describes how to use Mapping Learning as an application, whereas 
every other pages will help you to develop your own scripts using one or 
several of its module(s).

.. automodule:: maplearn.run_gui
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: maplearn.run
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: maplearn.run_example
    :members:
    :undoc-members:
    :show-inheritance:

Output
------

.. note::

    When processing is done, *maplearn* will show you the results in a 
    standardized report (HTML page), describing:

    * the dataset used
    * any pre-treatment(s)
    * applied algorithm(s)
    * statistical results (in the form of graphs and tables)
    * a synthesis comparing the result of the different algorithms


For your convenience, an example of output (in french) is available on 
`this link <./example_output/index.html>`_ .