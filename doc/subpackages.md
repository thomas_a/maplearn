.. Mapping Learning's Subpackages

Source code
===========

.. note::

    Mapping Learning is written in Python and uses major Open Source libraries, 
    like scikit-learn_ (Machine Learning algorithms),
    numpy_ and pandas_ to manipulate scientific data and Gdal_ to
    handle geographic data.

.. _scikit-learn: http://scikit-learn.org/
.. _numpy: http://www.numpy.org/
.. _pandas: https://pandas.pydata.org/
.. _Gdal: https://pypi.org/project/GDAL/

Modules
-------

Mapping Learning consists of 4 modules. The first 3 modules allow you to start
from your files and to obtain predictions (based on machine learning), and vice
versa. The fourth (*app*) is the "conductor", who drives the other parts of the
code.

.. image:: image/maplearn_code.svg
    :align: center
    :width: 500px

|

1. :mod:`maplearn.ml` : machine learning processing (and preprocessing)
2. :mod:`maplearn.datahandler` : to get/export a dataset usable in machine
   learning and the corresponding files
3. :mod:`maplearn.filehandler` : read/write a file
4. :mod:`maplearn.app` : application modules (configuration, ...)

.. toctree::

    maplearn.ml
    maplearn.datahandler
    maplearn.filehandler
    maplearn.app
