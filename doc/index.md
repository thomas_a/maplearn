.. Mapping Learning documentation master file, created by
   sphinx-quickstart on Thu Jul 19 18:35:05 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mapping Learning's documentation!
============================================

.. automodule:: maplearn
    :members:
    :undoc-members:

.. toctree::
    :caption: Table of Contents
    :maxdepth: 2
	
    install
    use
    subpackages
    contribute


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

