Mapping Learning
================


Mapping Learning (also called *maplearn*) makes use of machine learning easy 
(easier, at least). Initially designed for geographical data (cartography based
on remote sensing), Maplearn also deals very well with classical data (ie
tabular).

*NB: information in french is available in maplearn's* 
`wiki <https://bitbucket.org/thomas_a/maplearn/wiki/>`_ .

Features
--------

* **many algorithms** to make predictions (classification, clustering or
  regression)
* look for best hyper-parameters to **improve accuracy** of your results
* generalize machine learning's best practices (k-fold...)
* several preprocessing tasks available : reduction of dimensions...
* reads/writes **several file formats** (*geographic or not*)
* synthetizes results in a **standardized report**
* statiscal and more empirical **advices** will help novice users

Developpement
-------------


*Maplearn* is a **free software and library**, distributed under lGPL v3
license.

Written in Python, *maplearn* can be used whichever your operation system 
(Linux, Mac, Windows).

Dependancies
~~~~~~~~~~~~

* scikit-learn_ : Machine Learning algorithms
* numpy_ and pandas_ : to manipulate scientific data
* Gdal_ : to handle geographic data.

.. _scikit-learn: http://scikit-learn.org/
.. _numpy: http://www.numpy.org/
.. _pandas: https://pandas.pydata.org/
.. _Gdal: https://pypi.org/project/GDAL/
