#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Mapping Learning's GUI: about dialog


"""
from PyQt5 import QtCore, QtWidgets

from maplearn import __version__

class AboutDialog(QtWidgets.QDialog):
    """
    About Dialog
    """
    def __init__(self, parent=None):
        super(AboutDialog, self).__init__(parent, QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.FramelessWindowHint)
        self.setGeometry(500, 300, 250, 110)

        # Set window background color
        self.setAutoFillBackground(True)
        
#        Necessary? test on windows or set with stylesheet
        p = self.palette()
        p.setColor(self.backgroundRole(), QtCore.Qt.white)
        self.setPalette(p)

        self.layout = QtWidgets.QVBoxLayout()
        self.setWindowTitle("A propos")
        self.layout.setContentsMargins(20, 20, 20, 20)
        label = QtWidgets.QLabel()
        label.setStyleSheet("QLabel {color:#303030;}")
        _text = """<h1><img style="float:left;"
                src="./app/gui/Icon/logo.png">Mapping Learning</h1>
                <h2>Maplearn %s (alpha)</h2>
                <br>
                <p><i>maplearn</i> est un logiciel et une
                librairie libre (licence lGPL v3)</p>

                <p>Pour remonter des bugs ou demander une
                fonctionnalité, allez sur <br>
                <a href="https://bitbucket.org/thomas_a/maplearn/">
                https://bitbucket.org/thomas_a/maplearn/</a></p>

                <p>Documentation:
                <a href="https://maplearn.readthedocs.io/en/latest/">
                https://maplearn.readthedocs.io/en/latest/</a></p>

                <p>Copyright (c) Alban Thomas</p>
                """ % __version__

        label.setText(_text)
        label.setOpenExternalLinks(True)
        self.layout.addWidget(label)
        self.setLayout(self.layout)

    def mousePressEvent(self, e):
        """
        Close the About Dialog when the user clicks
        """
        self.close()

    def keyPressEvent(self, e):
        """
        Close the About Dialog when the user presses <ESC>
        """
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()
