#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 22:25:46 2019


Source (tuto with thread): https://riptutorial.com/fr/pyqt5/example/29500/barre-de-progression-de-base-pyqt

@author: thomas_a
"""
import os
import sys
from PyQt5 import QtCore, QtWidgets

from maplearn.app.main import Main
from maplearn.app.reporting import ReportWriter

class RunDialog(QtWidgets.QDialog):
    """
    Dialog to make user wait during calculations
    """
    def __init__(self, parent=None):
        super(RunDialog, self).__init__(parent, QtCore.Qt.WindowStaysOnTopHint)
        #| QtCore.Qt.FramelessWindowHint
        self.setGeometry(500, 300, 250, 110)
        # Set window background color
        self.setAutoFillBackground(True)
        
        # Necessary? test on windows or set with stylesheet
        p = self.palette()
        p.setColor(self.backgroundRole(), QtCore.Qt.white)
        self.setPalette(p)

        
        self.setWindowTitle("Veuillez patienter...")
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setContentsMargins(20, 20, 20, 20)
        self.__label = QtWidgets.QLabel()
        self.__label.setStyleSheet("QLabel {color:#303030;}")
        __text = """Veuillez patienter..."""
        self.__label.setText(__text)
        self.layout.addWidget(self.__label)
        self.setLayout(self.layout)
        self.show()
        
        self.refreshTimer = QtCore.QTimer(self)
        self.refreshTimer.setInterval(1000)
        self.refreshTimer.timeout.connect(self.update)
        self.refreshTimer.start(0)

    def run(self, config):
        
        # running application
        report_file = os.path.join(config.io['output'], 'index')
        report_writer = ReportWriter(report_file)
        sys.stdout = report_writer
        self.__label.setText("Initialisation...")
        
        appli = Main(config.io['output'], codes=config.codes,
                     **config.process)
    
        # TODO: PATCH tout moche à retirer dés que possible
        if config.process['type'] == 'clustering' and \
                config.io['samples'] is None:
            config.io['samples'] = config.io['data']
    
        self.__label.setText("Chargement des données...")
        appli.load(source=config.io['samples'], **config.io)
        if config.io['data'] is not None:
            appli.load_data(config.io['data'],
                            features=config.io['features'])
        self.__label.setText("Pretraitements...")
        appli.preprocess(**config.preprocess)
        self.__label.setText("Traiements...")
        appli.process(optimize=config.process['optimize'],
                      predict=config.process['predict'])
        self.show()
        self.__label.setText("Enregistrement du rapport...")
        report_writer.close()
        # export configuration in output folder
        config.write(os.path.join(config.io['output'], 'configuration.cfg'))
        self.close()