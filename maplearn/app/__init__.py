# -*- coding: utf-8 -*-
"""
**Application modules**


Modules necessary to Mapping Learning when it is used as an application :

* **config**: configuration
* **main**: the main class that uses other classes to process your data
* **reporting**: a module to format results in an html output
"""
