# -*- coding: utf-8 -*-
"""
**File handlers**


Read/write data from different kind of files

* **Csv**: tabular data as a text file
* **Excel**: tabular data as a Microsoft Excel file
* **Shapefile**: geographical vector file
* **ImageGeo**: geographical raster file
* *FileHandler*: abstract class to handle files

"""
