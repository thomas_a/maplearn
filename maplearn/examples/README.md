# EXEMPLES : fichiers de configuration

Le dossier contient des fichiers de configuration qui peuvent vous aider a
parametrer l'application pour vos propres besoins.

**ATTENTION**: les fichiers contiennent des chemins absolus qui necessitent
d'être redefinis avant de pouvoir les utiliser sur votre propre poste.