# -*- coding: utf-8 -*-
"""
**Data handlers**


Interim classes between file(s) and dataset

* **packdata**: creates a dataset with samples and data
* **labels**: labels associated to features (in samples)
* **loader**: loads data from a file or known datasets
* **writer**: writes data into a file
* **signature**: graphs a dataset
* **plotter**: generic class to make charts

"""